/*

Project:      Arduino Assignment 1: Blink & Fade
Student:      Nicole Vella
Course:       DGIF 2002: Physical Computing, OCAD University
Created on:   17/09/2019
Based on:     Arduino Lesson 1: Blink; Simon Monk; https://learn.adafruit.com/adafruit-arduino-lesson-1-blink
              In class presentation from week 2 of Lindy Wilkins' DGIF 2001 course at OCAD U

*/

int ledRed = 9; // Variable for the pin that the red led is connected to
int ledGreen = 10; // Variable for the pin that the green led is connected to
int ledBlue = 11; // Variable for the pin that the blue led is connected to


int longbeep = 750; // Variable for a long blink of light
int shortbeep = 250; // Variable for a short blink of light
int shortspace = 500; // Variable for a short pause between sections of a letter
int longspace = 1000; // Variable for a long pause between letter
int extralongspace = 2000; // Variable for a long pause between words


void setup() { // The setup function runs once when you press reset or power the board

  pinMode(ledRed, OUTPUT);    // Intialize the Red led which is connected to digital pin 9
  pinMode(ledGreen, OUTPUT);  // Intialize the Green led which is connected to digital pin 10
  pinMode(ledBlue, OUTPUT);   //  Intialize the Blue led which is connected to digital pin 11
}

void loop() {

  // Spell "SOS" In Morse Code
  // Morse Code Letter "S" Pattern
  digitalWrite(ledRed, HIGH); // Turn on red led
  delay(shortbeep);           // Pause/delay for amount of time declared for the shortbeep variable
  digitalWrite(ledRed, LOW);  // Turn off red led
  delay(shortspace);          // Pause/delay for amount of time declared for the shortspace variable
  digitalWrite(ledRed, HIGH); // Turn on the red led
  delay(shortbeep);           // Pause/delay for amount of time declared for the shortbeep variable
  digitalWrite(ledRed, LOW);  // Turn off the red led
  delay(shortspace);          // Pause/delay for amount of time declared for the shortspace variable
  digitalWrite(ledRed, HIGH); // Turn on red led
  delay(shortbeep);           // Pause/delay for amount of time declared for the shortbeep variable
  digitalWrite(ledRed, LOW);  // Turn off red led
  delay(longspace);           // Pause/delay for amount of time declared for the shortspace variable
  // End of Morse Code Letter "S" Pattern  

  // Morse Code Letter "O" Pattern
  digitalWrite(ledRed, HIGH);
  delay(longbeep);  
  digitalWrite(ledRed, LOW);
  delay(shortspace);
  digitalWrite(ledRed, HIGH);
  delay(longbeep);  
  digitalWrite(ledRed, LOW);
  delay(shortspace);
  digitalWrite(ledRed, HIGH);
  delay(longbeep);  
  digitalWrite(ledRed, LOW);
  delay(longspace); 
  // End of Morse Code Letter "O" Pattern  

  // Morse Code Letter "S" Pattern
  digitalWrite(ledRed, HIGH);
  delay(shortbeep);  
  digitalWrite(ledRed, LOW);
  delay(shortspace);
  digitalWrite(ledRed, HIGH);
  delay(shortbeep);  
  digitalWrite(ledRed, LOW);
  delay(shortspace);
  digitalWrite(ledRed, HIGH);
  delay(shortbeep);  
  digitalWrite(ledRed, LOW);
  delay(longspace);
  // End of Morse Code Letter "S" Pattern  





  // A longer delay between blink patterns
  delay(extralongspace);




  
  // Spell "Apple" In Morse Code
  // Morse Code Letter "A" Pattern
  digitalWrite(ledBlue, HIGH);    // Turn on blue led
  delay(shortbeep);               // Pause/delay for amount of time declared for the shortbeep variable
  digitalWrite(ledBlue, LOW);     // Turn off blue led
  delay(shortspace);              // Pause/delay for amount of time declared for the shortspace variable
  digitalWrite(ledBlue, HIGH);    // Turn on blue led
  delay(longbeep);                // Pause/delay for amount of time declared for the longbeep variable
  digitalWrite(ledBlue, LOW);     // Turn off blue led
  delay(longspace);               // Pause/delay for amount of time declared for the longspace variable
  // End of Morse Code Letter "A" Pattern 
  
  // Morse Code Letter "P" Pattern
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(longbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(longbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(longspace);
  // End of Morse Code Letter "P" Pattern 
  
  // Morse Code Letter "P" Pattern
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(longbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(longbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(longspace);
  // End of Morse Code Letter "P" Pattern 
  
  // Morse Code Letter "L" Pattern
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(longbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(shortspace);
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(longspace);
  // End of Morse Code Letter "L" Pattern 
  
  // Morse Code Letter "E" Pattern
  digitalWrite(ledBlue, HIGH);
  delay(shortbeep);
  digitalWrite(ledBlue, LOW);
  delay(longspace);
  // End of Morse Code Letter "E" Pattern 
  




  // A longer delay between blink patterns
  delay(extralongspace);




  
  // Spell "Pwned" In Morse Code
  // Morse Code Letter "P" Pattern
  digitalWrite(ledGreen, HIGH);    // Turn on green led
  delay(shortbeep);                // Pause/delay for amount of time declared for the shortbeep variable
  digitalWrite(ledGreen, LOW);     // Turn off green led
  delay(shortspace);               // Pause/delay for amount of time declared for the shortspace variable
  digitalWrite(ledGreen, HIGH);    // Turn on green led
  delay(longbeep);                 // Pause/delay for amount of time declared for the longbeep variable
  digitalWrite(ledGreen, LOW);     // Turn off green led
  delay(shortspace);               // Pause/delay for amount of time declared for the shortspace variable
  digitalWrite(ledGreen, HIGH);    // Turn on green led
  delay(longbeep);                 // Pause/delay for amount of time declared for the longbeep variable
  digitalWrite(ledGreen, LOW);     // Turn off green led
  delay(shortspace);               // Pause/delay for amount of time declared for the shortspace variable
  digitalWrite(ledGreen, HIGH);    // Turn on green led
  delay(shortbeep);                // Pause/delay for amount of time declared for the shortbeep variable
  digitalWrite(ledGreen, LOW);     // Turn off green led
  delay(longspace);                // Pause/delay for amount of time declared for the longspace variable
  // End of Morse Code Letter "P" Pattern 

  // Morse Code Letter "W" Pattern
  digitalWrite(ledGreen, HIGH);
  delay(shortbeep);
  digitalWrite(ledGreen, LOW);
  delay(shortspace);
  digitalWrite(ledGreen, HIGH);
  delay(longbeep);
  digitalWrite(ledGreen, LOW);
  delay(shortspace);
  digitalWrite(ledGreen, HIGH);
  delay(longbeep);
  digitalWrite(ledGreen, LOW);
  delay(longspace);  
  // End of Morse Code Letter "W" Pattern 

  // Morse Code Letter "N" Pattern
  digitalWrite(ledGreen, HIGH);
  delay(longbeep);
  digitalWrite(ledGreen, LOW);
  delay(shortspace);
  digitalWrite(ledGreen, HIGH);
  delay(shortbeep);
  digitalWrite(ledGreen, LOW);
  delay(longspace);
  // End of Morse Code Letter "N" Pattern 
  
  // Morse Code Letter "E" Pattern
  digitalWrite(ledGreen, HIGH);
  delay(shortbeep);
  digitalWrite(ledGreen, LOW);
  delay(longspace);
  // End of Morse Code Letter "E" Pattern 

  // Morse Code Letter "D" Pattern
  digitalWrite(ledGreen, HIGH);
  delay(longbeep);
  digitalWrite(ledGreen, LOW);
  delay(shortspace);
  digitalWrite(ledGreen, HIGH);
  delay(shortbeep);
  digitalWrite(ledGreen, LOW);
  delay(shortspace);
  digitalWrite(ledGreen, HIGH);
  delay(shortbeep);
  digitalWrite(ledGreen, LOW);
  delay(longspace);
  // End of Morse Code Letter "D" Pattern 

  

for (int e=0; e < 10; e++) {     // run the below for loops 10 times

  for (int i=0; i < 255; i++) {  // run this loop 255 times
    analogWrite(ledRed, i);       // set the brightness of the red led to the value of i (fade up)
    delay(1);                     // delay the iterative statement by 1 ms
  }

  for (int i=255; i > 0; i--) {  // run this loop 255 times
    analogWrite(ledRed, i);       // set the brightness of the red led to the value of i (fade down)
    delay(1);                     // delay the iterative statement by 1 ms
  }

  for (int i=0; i < 255; i++) {  // run this loop 255 times
    analogWrite(ledBlue, i);      // set the brightness of the blue led to the value of i (fade up)
    delay(1);                     // delay the iterative statement by 1 ms
  }

  for (int i=255; i > 0; i--) {  // run this loop 255 times
    analogWrite(ledBlue, i);       // set the brightness of the blue led to the value of i (fade down)
    delay(1);                     // delay the iterative statement by 1 ms
  }

  for (int i=0; i < 255; i++) {  // run this loop 255 times
    analogWrite(ledGreen, i);     // set the brightness of the green led to the value of i (fade up)
    delay(1);                     // delay the iterative statement by 1 ms
  }

  for (int i=255; i > 0; i--) {  // run this loop 255 times
    analogWrite(ledGreen, i);     // set the brightness of the green led to the value of i (fade down)
    delay(1);                     // delay the iterative statement by 1 ms
  }

}

  
 
}

